FROM java:8
ARG JAR_FILE=target/spamAppApplication.jar
ARG JAR_LIB_FILE=target/lib/

# cd /usr/local/runme
WORKDIR /usr/local/runme

# cp target/spamAppApplication.jar /usr/local/runme/spamAppApplication.jar
COPY ${JAR_FILE} spamAppApplication.jar

# cp -rf target/lib/  /usr/local/runme/lib
ADD ${JAR_LIB_FILE} lib/

# java -jar /usr/local/runme/spamAppApplication.jar
ENTRYPOINT ["java","-jar","spamAppApplication.jar"]