package com.example.dtos;

public class SpamDto {

    private boolean connected;
    private String now;

    public SpamDto() {
    }

    public boolean isConnected() {
        return connected;
    }

    public String getNow() {
        return now;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public void setNow(String now) {
        this.now = now;
    }
}
