package com.example.singleton;

import com.example.components.SpamComponent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
@Scope("singleton")
public class SingletonBean implements Serializable {

    private SpamComponent connectedUser;
    private SpamComponent notConnectedUser;

    public SingletonBean() {
        System.out.println("**SingletonBean initialyze");
        connectedUser = new SpamComponent(true);
        notConnectedUser = new SpamComponent(false);
    }

    public SpamComponent getConnectedUser() {
        return connectedUser;
    }

    public SpamComponent getNotConnectedUser() {
        return notConnectedUser;
    }

}
