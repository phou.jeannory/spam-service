package com.example.messages;

import com.example.dtos.SpamDto;
import com.example.singleton.SingletonBean;
import com.example.utils.StringBuilder;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Works only when user-app is down
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class SpamMessageTest {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private SingletonBean singletonBean;

    @Test
    public void testTenConnectedAndOneNotConnectedMessage() throws InterruptedException {

        purgeActiveMqMessage();

        //given 1
        sendMessage(true, "10-10-2020 23:19:50");

        //when 1
        final Object result1 = this.jmsTemplate.receiveAndConvert("spamIncrementQueueOut");

        //then 1
        Assert.assertEquals(StringBuilder.getConvertedMessages(singletonBean.getConnectedUser(), singletonBean.getNotConnectedUser()), result1.toString());

        //given 2
        sendMessage(true, "10-10-2020 23:19:51");

        //when 2
        final Object result2 = this.jmsTemplate.receiveAndConvert("spamIncrementQueueOut");

        //then 2
        Assert.assertEquals(StringBuilder.getConvertedMessages(singletonBean.getConnectedUser(), singletonBean.getNotConnectedUser()), result2.toString());

        //given 3
        sendMessage(true, "10-10-2020 23:19:52");

        //when 3
        final Object result3 = this.jmsTemplate.receiveAndConvert("spamIncrementQueueOut");

        //then 3
        Assert.assertEquals(StringBuilder.getConvertedMessages(singletonBean.getConnectedUser(), singletonBean.getNotConnectedUser()), result3.toString());

        //given 4
        sendMessage(false, "10-10-2020 23:19:53");

        //when 4
        final Object result4 = this.jmsTemplate.receiveAndConvert("spamIncrementQueueOut");

        //then 4
        Assert.assertEquals(StringBuilder.getConvertedMessages(singletonBean.getConnectedUser(), singletonBean.getNotConnectedUser()), result4.toString());

        //given 5
        sendMessage(true, "10-10-2020 23:19:55");

        //when 5
        final Object result5 = this.jmsTemplate.receiveAndConvert("spamIncrementQueueOut");

        //then 5
        Assert.assertEquals(StringBuilder.getConvertedMessages(singletonBean.getConnectedUser(), singletonBean.getNotConnectedUser()), result5.toString());

        //given 6
        sendMessage(true, "10-10-2020 23:19:55");

        //when 6
        final Object result6 = this.jmsTemplate.receiveAndConvert("spamIncrementQueueOut");

        //then 6
        Assert.assertEquals(StringBuilder.getConvertedMessages(singletonBean.getConnectedUser(), singletonBean.getNotConnectedUser()), result6.toString());

        //given 7
        sendMessage(true, "10-10-2020 23:19:55");

        //when 7
        final Object result7 = this.jmsTemplate.receiveAndConvert("spamIncrementQueueOut");

        //then 7
        Assert.assertEquals(StringBuilder.getConvertedMessages(singletonBean.getConnectedUser(), singletonBean.getNotConnectedUser()), result7.toString());

        //given 8
        sendMessage(true, "10-10-2020 23:19:55");

        //when 8
        final Object result8 = this.jmsTemplate.receiveAndConvert("spamIncrementQueueOut");

        //then 8
        Assert.assertEquals(StringBuilder.getConvertedMessages(singletonBean.getConnectedUser(), singletonBean.getNotConnectedUser()), result8.toString());

        //given 9
        sendMessage(true, "10-10-2020 23:19:55");

        //when 9
        final Object result9 = this.jmsTemplate.receiveAndConvert("spamIncrementQueueOut");

        //then 9
        Assert.assertEquals(StringBuilder.getConvertedMessages(singletonBean.getConnectedUser(), singletonBean.getNotConnectedUser()), result9.toString());

        //given 10
        sendMessage(true, "10-10-2020 23:19:55");

        //when 10
        final Object result10 = this.jmsTemplate.receiveAndConvert("spamIncrementQueueOut");

        //then 10
        Assert.assertEquals(StringBuilder.getConvertedMessages(singletonBean.getConnectedUser(), singletonBean.getNotConnectedUser()), result10.toString());

        //given 11
        //implicit sending message
        sendMessage(true, "10-10-2020 23:19:55");

        //when 11
        final Object result11 = this.jmsTemplate.receiveAndConvert("spamIncrementQueueOut");

        //then 11
        Assert.assertEquals(StringBuilder.getConvertedMessages(singletonBean.getConnectedUser(), singletonBean.getNotConnectedUser()), result11.toString());
        Assert.assertTrue(10 == singletonBean.getConnectedUser().getCount());
        Assert.assertTrue(1 == singletonBean.getNotConnectedUser().getCount());
        //proof of sending message
        Assert.assertEquals("les utilisateurs connectés vous ont spam 10 fois depuis le 10-10-2020 23:19:50", singletonBean.getConnectedUser().message());
    }

    private void purgeActiveMqMessage(){
        this.jmsTemplate.setReceiveTimeout(5000);
        int i = 0;
        try {
            while (i < 5000) {
                System.out.println("purge start");
                System.out.println("" + i + " - " + this.jmsTemplate.receiveAndConvert("spamIncrementQueueOut").toString());
                i += 1;
            }
        }catch (NullPointerException ex){
            System.out.println("purge is over");
        }
    }

    private void sendMessage(boolean isConnected, String date) throws InterruptedException {
        final SpamDto spamDto = new SpamDto();
        spamDto.setConnected(isConnected);
        spamDto.setNow(date);
        sendMessage(spamDto);
    }

    private void sendMessage(SpamDto spamDto) throws InterruptedException {
        jmsTemplate.convertAndSend("spamIncrement", spamDto);
        jmsTemplate.setReceiveTimeout(2_000);
        Thread.sleep(2000);
    }

}