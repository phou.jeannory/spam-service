package com.example.messages;

import com.example.dtos.SpamDto;
import com.example.singleton.SingletonBean;
import com.google.gson.Gson;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.listener.adapter.JmsResponse;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import java.util.Arrays;

@Component
public class SpamMessage {

    @Autowired
    private SingletonBean singletonBean;

    @Autowired
    private final SimpMessagingTemplate template;

    @Autowired
    private Gson gson;

    public SpamMessage(SimpMessagingTemplate template) {
        this.template = template;
    }

    @JmsListener(destination = "spamIncrement", containerFactory = "myFactory")
    public JmsResponse getSpamIncrement(Message message) throws JMSException {
        ActiveMQTextMessage amqMessage = (ActiveMQTextMessage) message;
        final String messageResponse = amqMessage.getText();
        final SpamDto spamDto = gson.fromJson(messageResponse, SpamDto.class);
        updateAndSend(spamDto);
        return JmsResponse.forQueue(Arrays.asList(singletonBean.getConnectedUser(), singletonBean.getNotConnectedUser()), "spamIncrementQueueOut");
    }

    private void updateAndSend(SpamDto spamDto) {
        if (spamDto.isConnected()) {
            singletonBean.getConnectedUser().increment(spamDto.getNow());
            if (isSendNotification(singletonBean.getConnectedUser().getCount())) {
                this.template.convertAndSend("/message", singletonBean.getConnectedUser().message());
            }
        } else {
            singletonBean.getNotConnectedUser().increment(spamDto.getNow());
            if (isSendNotification(singletonBean.getNotConnectedUser().getCount())) {
                this.template.convertAndSend("/message", singletonBean.getNotConnectedUser().message());
            }
        }
    }

    private boolean isSendNotification(int count) {
        return count % 10 == 0;
    }

}
