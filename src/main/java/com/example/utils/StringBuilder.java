package com.example.utils;

import com.example.components.SpamComponent;

public class StringBuilder {

    private static String getConvertedMessage(SpamComponent spamComponent){
        return "{count=" + spamComponent.getCount() + ", since=" + spamComponent.getSince() + ", connected=" + spamComponent.isConnected() + "}";
    }

    public static String getConvertedMessages(SpamComponent connectedUser, SpamComponent notConnectedUser){
        return "["+getConvertedMessage(connectedUser)+", "+getConvertedMessage(notConnectedUser)+"]";
    }
}
