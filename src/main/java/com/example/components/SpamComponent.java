package com.example.components;

import java.io.Serializable;

public class SpamComponent implements Serializable {

    private int count = 0;
    private String since;
    private final boolean isConnected;

    public SpamComponent(boolean isConnected) {
        this.isConnected = isConnected;
    }

    public void increment(final String now) {
        count += 1;
        updateSince(now);
    }

    public Integer getCount() {
        return count;
    }

    public void updateSince(final String now) {
        if (count == 1 || since.isEmpty()) {
            since = now;
        }
    }

    public String getSince() {
        return since;
    }

    public String message() {
        String isConnected = "";
        if(!this.isConnected){
            isConnected = "non ";
        }
        return "les utilisateurs " + isConnected + "connectés vous ont spam " + this.getCount() + " fois depuis le " + this.getSince();
    }

    public boolean isConnected() {
        return isConnected;
    }

}
